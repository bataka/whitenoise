//+----------------------------------------------------------------------------+
//|                                                          SetOrderSell.mq4  |
//|                                                                            |
//|                                                    ��� ����� �. aka KimIV  |
//|                                                       http://www.kimiv.ru  |
//|                                                                            |
//|  17.07.2008  ������ ������������� ���������� ����� SELL.                   |
//+----------------------------------------------------------------------------+
#property copyright "��� ����� �. aka KimIV"
#property link      "http://www.kimiv.ru"
#property show_inputs
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+


//////////->SETUP LOT HERE<-/////////////////////////
double Divider = 10;
enum lot
  {
   z=1,//1
   q=2,//2
   w=4,//3
   e=7,//4
   r=10,//5
   t=14,//6
   y=24,//7
  };  
input lot Lot=1;
//////////->END SETUP <-/////////////////////////     

//------- ������� ��������� ������� -------------------------------------------+
extern int    MagicNumber=3310;         // ������������� ������
extern int    StopLoss    = 0;         // ������ ����� � �������
extern int    TakeProfit  = 0;         // ������ ����� � �������
extern int    Slippage    = 3;         // ��������������� ����
extern int    NumberOfTry = 3;         // ���������� �������� �������

//------- ���������� ���������� ������� ---------------------------------------+
bool   gbDisabled    = False;          // ����������
bool   UseSound      = True;           // ������������ �������� ������
string SoundSuccess  = "ok.wav";       // ���� ������
string SoundError    = "timeout.wav";  // ���� ������
color  clOpenBuy     = clrYellow;      // ���� ������ �������� Buy
color  clOpenSell=clrYellow;     // ���� ������ �������� Sell

//------- ����������� ������� ������� ------------------------------------------
#include <stdlib.mqh>        // ����������� ���������� ��4
//+----------------------------------------------------------------------------+
//|  script program start function                                             |
//+----------------------------------------------------------------------------+
void start()
  {
//double Lots=(double) Lot/10;
   double Lots=NormalizeDouble(double(Lot)/Divider,2);
   double pp=WindowPriceOnDropped();
   double sl=0, tp=0;

   if(pp>0)
     {
      if(pp>Bid)
        {
         if(StopLoss  >0) sl=pp+StopLoss  *Point; else sl=0;
         if(TakeProfit>0) tp=pp-TakeProfit*Point; else tp=0;
         SetOrder(NULL,OP_SELLLIMIT,Lots,pp,sl,tp,MagicNumber);
        }
      if(pp<Bid)
        {
         if(StopLoss  >0) sl=pp+StopLoss  *Point; else sl=0;
         if(TakeProfit>0) tp=pp-TakeProfit*Point; else tp=0;
         SetOrder(NULL,OP_SELLSTOP,Lots,pp,sl,tp,MagicNumber);
        }
     }
   else Message("�� ������� ���������� ���� ��������� ������!");
  }
//+----------------------------------------------------------------------------+
//|  �����    : ��� ����� �. aka KimIV,  http://www.kimiv.ru                   |
//+----------------------------------------------------------------------------+
//|  ������   : 12.03.2008                                                     |
//|  �������� : ���������� ���� ������������� �������.                         |
//+----------------------------------------------------------------------------+
//|  ���������:                                                                |
//|    sy - ������������ �����������   (""   - ����� ������,                   |
//|                                     NULL - ������� ������)                 |
//|    op - ��������                   (-1   - ����� �����)                    |
//|    mn - MagicNumber                (-1   - ����� �����)                    |
//|    ot - ����� ��������             ( 0   - ����� ����� ���������)          |
//+----------------------------------------------------------------------------+
bool ExistOrders(string sy="",int op=-1,int mn=-1,datetime ot=0)
  {
   int i,k=OrdersTotal(),ty;

   if(sy=="0") sy=Symbol();
   for(i=0; i<k; i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES))
        {
         ty=OrderType();
         if(ty>1 && ty<6)
           {
            if((OrderSymbol()==sy || sy=="") && (op<0 || ty==op))
              {
               if(mn<0 || OrderMagicNumber()==mn)
                 {
                  if(ot<=OrderOpenTime()) return(True);
                 }
              }
           }
        }
     }
   return(False);
  }
//+----------------------------------------------------------------------------+
//|  �����    : ��� ����� �. aka KimIV,  http://www.kimiv.ru                   |
//+----------------------------------------------------------------------------+
//|  ������   : 01.09.2005                                                     |
//|  �������� : ���������� ������������ �������� ��������                      |
//+----------------------------------------------------------------------------+
//|  ���������:                                                                |
//|    op - ������������� �������� ��������                                    |
//+----------------------------------------------------------------------------+
string GetNameOP(int op)
  {
   switch(op)
     {
      case OP_BUY      : return("Buy");
      case OP_SELL     : return("Sell");
      case OP_BUYLIMIT : return("BuyLimit");
      case OP_SELLLIMIT: return("SellLimit");
      case OP_BUYSTOP  : return("BuyStop");
      case OP_SELLSTOP : return("SellStop");
      default          : return("Unknown Operation");
     }
  }
//+----------------------------------------------------------------------------+
//|  �����    : ��� ����� �. aka KimIV,  http://www.kimiv.ru                   |
//+----------------------------------------------------------------------------+
//|  ������   : 01.09.2005                                                     |
//|  �������� : ���������� ������������ ����������                             |
//+----------------------------------------------------------------------------+
//|  ���������:                                                                |
//|    TimeFrame - ��������� (���������� ������)      (0 - ������� ��)         |
//+----------------------------------------------------------------------------+
string GetNameTF(int TimeFrame=0)
  {
   if(TimeFrame==0) TimeFrame=Period();
   switch(TimeFrame)
     {
      case PERIOD_M1:  return("M1");
      case PERIOD_M5:  return("M5");
      case PERIOD_M15: return("M15");
      case PERIOD_M30: return("M30");
      case PERIOD_H1:  return("H1");
      case PERIOD_H4:  return("H4");
      case PERIOD_D1:  return("Daily");
      case PERIOD_W1:  return("Weekly");
      case PERIOD_MN1: return("Monthly");
      default:         return("UnknownPeriod");
     }
  }
//+----------------------------------------------------------------------------+
//|  �����    : ��� ����� �. aka KimIV,  http://www.kimiv.ru                   |
//+----------------------------------------------------------------------------+
//|  ������   : 01.09.2005                                                     |
//|  �������� : ����� ��������� � ������� � � ������                           |
//+----------------------------------------------------------------------------+
//|  ���������:                                                                |
//|    m - ����� ���������                                                     |
//+----------------------------------------------------------------------------+
void Message(string m)
  {
   Comment(m);
   if(StringLen(m)>0) Print(m);
  }
//+----------------------------------------------------------------------------+
//|  �����    : ��� ����� �. aka KimIV,  http://www.kimiv.ru                   |
//+----------------------------------------------------------------------------+
//|  ������   : 13.03.2008                                                     |
//|  �������� : ��������� ������.                                              |
//+----------------------------------------------------------------------------+
//|  ���������:                                                                |
//|    sy - ������������ �����������   (NULL ��� "" - ������� ������)          |
//|    op - ��������                                                           |
//|    ll - ���                                                                |
//|    pp - ����                                                               |
//|    sl - ������� ����                                                       |
//|    tp - ������� ����                                                       |
//|    mn - Magic Number                                                       |
//|    ex - ���� ���������                                                     |
//+----------------------------------------------------------------------------+
void SetOrder(string sy,int op,double ll,double pp,
              double sl=0,double tp=0,int mn=0,datetime ex=0)
  {
   color    clOpen;
   datetime ot;
   double   pa,pb,mp;
   int      err,it,ticket,msl;
   string   lsComm=WindowExpertName()+" "+GetNameTF(Period());

   if(sy=="" || sy=="0") sy=Symbol();
   msl=MarketInfo(sy,MODE_STOPLEVEL);
   if(op==OP_BUYLIMIT || op==OP_BUYSTOP) clOpen=clOpenBuy; else clOpen=clOpenSell;
   if(ex>0 && ex<TimeCurrent()) ex=0;
   for(it=1; it<=NumberOfTry; it++)
     {
      if(!IsTesting() && (!IsExpertEnabled() || IsStopped()))
        {
         Print("SetOrder(): ��������� ������ �������");
         break;
        }
      while(!IsTradeAllowed()) Sleep(5000);
      RefreshRates();
      ot=TimeCurrent();
      ticket=OrderSend(sy,op,ll,pp,Slippage,sl,tp,lsComm,mn,ex,clOpen);
      if(ticket>0)
        {
         if(UseSound) PlaySound(SoundSuccess); break;
           } else {
         err=GetLastError();
         if(err==128 || err==142 || err==143)
           {
            Sleep(1000*66);
            if(ExistOrders(sy,op,mn,ot))
              {
               if(UseSound) PlaySound(SoundSuccess); break;
              }
            Print("Error(",err,") set order: ",ErrorDescription(err),", try ",it);
            continue;
           }
         if(UseSound) PlaySound(SoundError);
         mp=MarketInfo(sy, MODE_POINT);
         pa=MarketInfo(sy, MODE_ASK);
         pb=MarketInfo(sy, MODE_BID);
         if(pa==0 && pb==0) Message("SetOrder(): ��������� � ������ ����� ������� ������� "+sy);
         Print("Error(",err,") set order: ",ErrorDescription(err),", try ",it);
         Print("Ask=",pa,"  Bid=",pb,"  sy=",sy,"  ll=",ll,"  op=",GetNameOP(op),
               "  pp=",pp,"  sl=",sl,"  tp=",tp,"  mn=",mn);
         // ������������ �����
         if(err==130)
           {
            Sleep(1000*5.3);
            switch(op)
              {
               case OP_BUYLIMIT:
                  if(pp>pa-msl*mp) pp=pa-msl*mp;
                  if(sl>pp-(msl+1)*mp) sl=pp-(msl+1)*mp;
                  if(tp>0 && tp<pp+(msl+1)*mp) tp=pp+(msl+1)*mp;
                  break;
               case OP_BUYSTOP:
                  if(pp<pa+(msl+1)*mp) pp=pa+(msl+1)*mp;
                  if(sl>pp-(msl+1)*mp) sl=pp-(msl+1)*mp;
                  if(tp>0 && tp<pp+(msl+1)*mp) tp=pp+(msl+1)*mp;
                  break;
               case OP_SELLLIMIT:
                  if(pp<pb+msl*mp) pp=pb+msl*mp;
                  if(sl>0 && sl<pp+(msl+1)*mp) sl=pp+(msl+1)*mp;
                  if(tp>pp-(msl+1)*mp) tp=pp-(msl+1)*mp;
                  break;
               case OP_SELLSTOP:
                  if(pp>pb-msl*mp) pp=pb-msl*mp;
                  if(sl>0 && sl<pp+(msl+1)*mp) sl=pp+(msl+1)*mp;
                  if(tp>pp-(msl+1)*mp) tp=pp-(msl+1)*mp;
                  break;
              }
            Print("SetOrder(): ��������������� ������� ������");
            continue;
           }
         // ���������� ������ ���������
         if(err==2 || err==64 || err==65 || err==133)
           {
            gbDisabled=True; break;
           }
         // ���������� �����
         if(err==4 || err==131 || err==132)
           {
            Sleep(1000*300); break;
           }
         // ������� ������ ������� (8) ��� ������� ����� �������� (141)
         if(err==8 || err==141) Sleep(1000*100);
         if(err==139 || err==140 || err==148) break;
         // �������� ������������ ���������� ��������
         if(err==146) while(IsTradeContextBusy()) Sleep(1000*11);
         // ��������� ���� ���������
         if(err==147)
           {
            ex=0; continue;
           }
         if(err!=135 && err!=138) Sleep(1000*7.7);
        }
     }
  }
//+----------------------------------------------------------------------------+
