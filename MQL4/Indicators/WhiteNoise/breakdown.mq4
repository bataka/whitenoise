//+-----------------------------------------------------------------+
//|                                                    breakdown.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//#property indicator_chart_window
#include <WinUser32.mqh>

int cock=0;
int trigger=0;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {

   double ST=iStochastic(NULL,0,12,8,5,3,1,0,0);
   double DH = NormalizeDouble(iCustom(NULL, 5, "Donchian_Channels_-_Generalized_version1", 24, 3, -2, 0, 700, 1, 0), Digits);
   double DL = NormalizeDouble(iCustom(NULL, 5, "Donchian_Channels_-_Generalized_version1", 24, 3, -2, 0, 700, 0, 0), Digits);
   double HH = NormalizeDouble(iCustom(NULL,5,"Heiken Ashi", 0, 0), Digits);
   double HL = NormalizeDouble(iCustom(NULL,5,"Heiken Ashi", 1, 0), Digits);

   double ST1=iStochastic(NULL,0,12,8,5,3,1,0,1);
   double DH1 = NormalizeDouble(iCustom(NULL, 5, "Donchian_Channels_-_Generalized_version1", 24, 3, -2, 0, 700, 1, 1), Digits);
   double DL1 = NormalizeDouble(iCustom(NULL, 5, "Donchian_Channels_-_Generalized_version1", 24, 3, -2, 0, 700, 0, 1), Digits);
   double HH1 = NormalizeDouble(iCustom(NULL,5,"Heiken Ashi", 0, 1), Digits);
   double HL1 = NormalizeDouble(iCustom(NULL,5,"Heiken Ashi", 1, 1), Digits);

//cock upprice
   if(
      ((DH<HL || DH<HH) && (DL<HL && DL<HH)) && (cock==-1 || cock==0))
     {
      //Comment("upprice");
      cock=1;
     }

//cock downprice
   if((DL>HL || DL>HH) && (DH>HL && DH>HH) && (cock==1 || cock==0))
     {
      //Comment("downprice");
      cock=-1;
     }

//trigger up
   if(cock==1 && ST>80 && ST<ST1 && (trigger==0 || trigger==-1))
     {
      trigger=1;
      Signal("sell");
      if(IsTesting())
        {
/*        
         if(ChartApplyTemplate(0,"\\tester.tpl"))
            Print("��������� ������� ������ 'my_template.tpl'");
         else
            Print("�� ������� ��������� ������ 'my_template.tpl', ������ ",GetLastError());
*/
         pause();
        }

     }

//trigger down
   if(cock==-1 && ST<20 && ST>ST1 && (trigger==0 || trigger==1))
     {
      trigger=-1;
      Signal("buy");
      if(IsTesting())
        {
/*
         if(ChartApplyTemplate(0,"\\tester.tpl"))
            Print("��������� ������� ������ 'my_template.tpl'");
         else          
            Print("�� ������� ��������� ������ 'my_template.tpl', ������ ",GetLastError());
*/
         pause();
        }

     }

// �������� ������ �� ������ �����
   if(DH1>HH1 && DH1>HL1 && DL1<HH1 && DL1<HL1)
      cock=0;

   if(ST1>20 && ST1<80)
      trigger=0;

//--- return value of prev_calculated for next call
   return(rates_total);

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void pause()
  {
   Print("Pause");
   keybd_event(19,0,0,0);
   Sleep(10);
   keybd_event(19,0,2,0);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Signal(string direction)
  {
//��������� ����� �� ����������
   datetime time=TimeCurrent();
   string strtime=TimeToStr(time);
//�������� �����
   ObjectCreate(strtime,OBJ_VLINE,0,iTime(NULL,0,0),0);// ��������� ����
   ObjectSet(strtime,OBJPROP_STYLE,STYLE_DASH);
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(direction=="buy")
     {
      ObjectSet(strtime,OBJPROP_COLOR,clrGreen);
      Alert(Symbol()+" -> "+direction); // ���������
                                        //PlaySound("valk.wav");
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(direction=="sell")
     {
      ObjectSet(strtime,OBJPROP_COLOR,clrRed);
      Alert(Symbol()+" -> "+direction); // ���������
                                        //PlaySound("valk.wav");
     }
  }
//+------------------------------------------------------------------+
