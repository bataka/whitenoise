//+------------------------------------------------------------------+
//|                                                 ClearPending.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
//   Alert("tick");
   int tickets[];
   int ticket;
   int j=OrdersTotal();
   for(int i=j-1; i>=0; i--)
     {
      if(!OrderSelect(i,SELECT_BY_POS,MODE_TRADES)) continue;
      if(OrderType()==OP_BUY || OrderType()==OP_SELL) continue;
      ticket=OrderTicket();
      if(Trigger(OrderSymbol()))
        {
         ArrayResize(tickets,ArraySize(tickets)+1);
         tickets[ArraySize(tickets)-1]=ticket;
        }
     }
   for(int k=0; ArraySize(tickets)>k; k++)
     {
      if(!OrderDelete(tickets[k],clrRed)) Alert(GetLastError(),Symbol());
      //            Alert(tickets[k]);
     }

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool Trigger(string pair)
  {
   bool res=true;
   int j=OrdersTotal();
   for(int i=j-1; i>=0; i--)
     {
      if(!OrderSelect(i,SELECT_BY_POS,MODE_TRADES)) continue;
      if(OrderSymbol()==pair && (OrderType()==OP_BUY || OrderType()==OP_SELL)) res=false;
      if(OrderSymbol()==pair && OrderStopLoss()>0) return(true);
      //      Alert(pair);
      //      if(OrderStopLoss()>0) return(true);
     }
   return res;
  }
//+------------------------------------------------------------------+
