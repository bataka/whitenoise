//+------------------------------------------------------------------+
//|                                                   TrailByATR.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

input int magic=3310;
input int atr_timeframe=5;
input int atr1_period= 5;
input int atr1_shift = 1;
input int atr2_period= 20;
input int atr2_shift = 1;
input double coeff=1;
input bool trlinloss=false;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   int total=OrdersTotal();
   int res;

   if(total<2 && IsTesting())
     {
      res=OrderSend(Symbol(),OP_SELL,0.01,Bid,50,0,0,"",1,0,Red);
      res=OrderSend(Symbol(),OP_BUY,0.01,Ask,50,0,0,"",1,0,Red);

     }
   total=OrdersTotal();
   for(int pos=0;pos<total;pos++)
     {
      if(OrderSelect(pos,SELECT_BY_POS,MODE_TRADES)==false) continue;      
      if(OrderMagicNumber()!=magic) continue;
      int ticket=OrderTicket();
      TrailingByATR(ticket);
     }
  }
//+------------------------------------------------------------------+

void TrailingByATR(int ticket)
  {
// проверяем переданные значения   
   if((ticket==0) || (atr1_period<1) || (atr2_period<1) || (coeff<=0) || (!OrderSelect(ticket,SELECT_BY_TICKET)) || 
      ((atr_timeframe!=1) && (atr_timeframe!=5) && (atr_timeframe!=15) && (atr_timeframe!=30) && (atr_timeframe!=60) && 
      (atr_timeframe!=240) && (atr_timeframe!=1440) && (atr_timeframe!=10080) && (atr_timeframe!=43200)) || (atr1_shift<0) || (atr2_shift<0))
     {
      Print("Трейлинг функцией TrailingByATR() невозможен из-за некорректности значений переданных ей аргументов.");
     }

   double curr_atr1; // текущее значение ATR - 1
   double curr_atr2; // текущее значение ATR - 2
   double best_atr; // большее из значений ATR
   double atrXcoeff; // результат умножения большего из ATR на коэффициент
   double newstop; // новый стоплосс

   string mySymbol=OrderSymbol();
   double myPoint = MarketInfo(mySymbol,MODE_POINT);
   double mySpread= MarketInfo(mySymbol,MODE_SPREAD);
   double myBid = MarketInfo(mySymbol,MODE_BID);
   double myAsk = MarketInfo(mySymbol,MODE_ASK);      


// текущее значение ATR-1, ATR-2
   curr_atr1 = iATR(mySymbol,atr_timeframe,atr1_period,atr1_shift);
   curr_atr2 = iATR(mySymbol,atr_timeframe,atr2_period,atr2_shift);

// большее из значений
   best_atr=MathMax(curr_atr1,curr_atr2);

// после умножения на коэффициент
   atrXcoeff=best_atr*coeff;

// если длинная позиция (OP_BUY)
   if(OrderType()==OP_BUY)
     {
      // откладываем от текущего курса (новый стоплосс)
      newstop=myBid-atrXcoeff;

      // если trlinloss==true (т.е. следует тралить в зоне лоссов), то
      if(trlinloss==true)
        {
         // если стоплосс неопределен, то тралим в любом случае
         if((OrderStopLoss()==0) && (newstop<myBid-MarketInfo(mySymbol,MODE_STOPLEVEL)*myPoint))
           {
            if(!OrderModify(ticket,OrderOpenPrice(),newstop,OrderTakeProfit(),OrderExpiration()))
               Print("Не удалось модифицировать ордер №",OrderTicket(),". Ошибка: ",GetLastError());
           }
         // иначе тралим только если новый стоп лучше старого
         else
           {
            if((newstop>OrderStopLoss()) && (newstop<myBid-MarketInfo(mySymbol,MODE_STOPLEVEL)*myPoint))
               if(!OrderModify(ticket,OrderOpenPrice(),newstop,OrderTakeProfit(),OrderExpiration()))
                  Print("Не удалось модифицировать ордер №",OrderTicket(),". Ошибка: ",GetLastError());
           }
        }
      else
        {
         // если стоплосс неопределен, то тралим, если стоп лучше открытия
         if((OrderStopLoss()==0) && (newstop>OrderOpenPrice()) && (newstop<myBid-MarketInfo(mySymbol,MODE_STOPLEVEL)*myPoint))
           {
            if(!OrderModify(ticket,OrderOpenPrice(),newstop,OrderTakeProfit(),OrderExpiration()))
               Print("Не удалось модифицировать ордер №",OrderTicket(),". Ошибка: ",GetLastError());
           }
         // если стоп не равен 0, то меняем его, если он лучше предыдущего и курса открытия
         else
           {
            if((newstop>OrderStopLoss()) && (newstop>OrderOpenPrice()) && (newstop<myBid-MarketInfo(mySymbol,MODE_STOPLEVEL)*myPoint))
               if(!OrderModify(ticket,OrderOpenPrice(),newstop,OrderTakeProfit(),OrderExpiration()))
                  Print("Не удалось модифицировать ордер №",OrderTicket(),". Ошибка: ",GetLastError());
           }
        }
     }

// если короткая позиция (OP_SELL)
   if(OrderType()==OP_SELL)
     {
      // откладываем от текущего курса (новый стоплосс)
      newstop=myAsk+atrXcoeff;

      // если trlinloss==true (т.е. следует тралить в зоне лоссов), то
      if(trlinloss==true)
        {
         // если стоплосс неопределен, то тралим в любом случае
         if((OrderStopLoss()==0) && (newstop>myAsk+MarketInfo(mySymbol,MODE_STOPLEVEL)*myPoint))
           {
            if(!OrderModify(ticket,OrderOpenPrice(),newstop,OrderTakeProfit(),OrderExpiration()))
               Print("Не удалось модифицировать ордер №",OrderTicket(),". Ошибка: ",GetLastError());
           }
         // иначе тралим только если новый стоп лучше старого
         else
           {
            if((newstop<OrderStopLoss()) && (newstop>myAsk+MarketInfo(mySymbol,MODE_STOPLEVEL)*myPoint))
               if(!OrderModify(ticket,OrderOpenPrice(),newstop,OrderTakeProfit(),OrderExpiration()))
                  Print("Не удалось модифицировать ордер №",OrderTicket(),". Ошибка: ",GetLastError());
           }
        }
      else
        {
         // если стоплосс неопределен, то тралим, если стоп лучше открытия
         if((OrderStopLoss()==0) && (newstop<OrderOpenPrice()) && (newstop>myAsk+MarketInfo(mySymbol,MODE_STOPLEVEL)*myPoint))
           {
            if(!OrderModify(ticket,OrderOpenPrice(),newstop,OrderTakeProfit(),OrderExpiration()))
               Print("Не удалось модифицировать ордер №",OrderTicket(),". Ошибка: ",GetLastError());
           }
         // если стоп не равен 0, то меняем его, если он лучше предыдущего и курса открытия
         else
           {
            if((newstop<OrderStopLoss()) && (newstop<OrderOpenPrice()) && (newstop>myAsk+MarketInfo(mySymbol,MODE_STOPLEVEL)*myPoint))
               if(!OrderModify(ticket,OrderOpenPrice(),newstop,OrderTakeProfit(),OrderExpiration()))
                  Print("Не удалось модифицировать ордер №",OrderTicket(),". Ошибка: ",GetLastError());
           }
        }
     }
  }
//+------------------------------------------------------------------+
