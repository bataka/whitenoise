//+------------------------------------------------------------------+
//|                                                    BreakEven.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

input int SL=1;
input int STEP=0;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   if(OrdersTotal()<2 && IsTesting())
     {
      if(OrderSend(Symbol(),OP_SELL,0.01,Bid,50,0,0,"",1,0,Red))
         Print("Open order");
      if(OrderSend(Symbol(),OP_BUY,0.01,Ask,50,0,0,"",1,0,Red))
         Print("Open order");
     }
   BreakEven();
  }
//+------------------------------------------------------------------+

void BreakEven()
  {
   double Freeze,sl;
   for(int pos=0;pos<=OrdersTotal();pos++)
     {
      if(OrderSelect(pos,SELECT_BY_POS,MODE_TRADES)==false) continue;
      if(OrderStopLoss()>0) continue;

      string mySymbol= OrderSymbol();
      double myPoint = MarketInfo(mySymbol,MODE_POINT);
      double mySpread= MarketInfo(mySymbol,MODE_SPREAD);
      double myBid = MarketInfo(mySymbol,MODE_BID);
      double myAsk = MarketInfo(mySymbol,MODE_ASK);

      Freeze=2*mySpread+STEP;
      //Freeze=mySpread+STEP;

      if(OrderType()==OP_BUY)
        {
         if(OrderOpenPrice()+myPoint*Freeze<myBid)
           {            
            sl=NormalizeDouble(OrderOpenPrice()+myPoint*SL,Digits);
            if(OrderModify(OrderTicket(),OrderOpenPrice(),sl,0,0,clrRed))
              {
               Print("Setup noloss sucsess ",mySymbol);
              }
            else
              {
               Print("ERROR noloss ",mySymbol," ",GetLastError());
              }
           }
        }
      if(OrderType()==OP_SELL)
        {         
         if(OrderOpenPrice()-myPoint*Freeze>myAsk)
           {
            sl=NormalizeDouble(OrderOpenPrice()-myPoint*SL,Digits);
            if(OrderModify(OrderTicket(),OrderOpenPrice(),sl,0,0,clrRed))
              {
               Print("Setup noloss sucsess ",mySymbol);
              }
            else
              {
               Print("ERROR noloss ",mySymbol," ",GetLastError());
              }
           }
        }
     }
  }
//+------------------------------------------------------------------+
